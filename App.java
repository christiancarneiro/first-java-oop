import java.time.LocalDate;

public class App {
    public static void main(String[] args) {
        System.out.println("\nPrimeiro exemplo - Venda");
        RealState houseOfDragon = new RealState(3, 43, 4453.32F, "rua dos alfeneiros 321", true);
        System.out.println("Endereço: " + houseOfDragon.getAdress());
        System.out.println("Area construída: " + houseOfDragon.getNumPavements());
        System.out.println("Área construída: " + houseOfDragon.getArea());
        System.out.println("Número de pavimentos: " + houseOfDragon.getArea());
        System.out.println("Número de comodos: " + houseOfDragon.getNumRooms());
        System.out.println("Está disponível: " + houseOfDragon.getAvailable());
        System.out.println("Indisponível até: " + houseOfDragon.getUnavailableUntil() + "\n\n");

        houseOfDragon.sell();
        System.out.println("Está disponível: " + houseOfDragon.getAvailable());
        System.out.println("Indisponível até: " + houseOfDragon.getUnavailableUntil() + "\n\n");

        System.out.println("\nTentando vendê-la de novo...");
        houseOfDragon.sell();

        // ########################################################################################
        // ########################################################################################
        // ########################################################################################
        // ########################################################################################

        System.out.println("\nSegundo exemplo - Aluguel");
        RealState houseOfCards = new RealState(2, 44, 4213.32F, "rua dos alfeneiros 777", true);
        System.out.println("Endereço: " + houseOfCards.getAdress());
        System.out.println("Area construída: " + houseOfCards.getNumPavements());
        System.out.println("Número de pavimentos: " + houseOfCards.getArea());
        System.out.println("Número de comodos: " + houseOfCards.getNumRooms());
        System.out.println("Está disponível: " + houseOfCards.getAvailable());
        System.out.println("Indisponível até: " + houseOfCards.getUnavailableUntil() + "\n\n");

        LocalDate until = LocalDate.parse("2012-06-30");
        System.out.println("\nTentando alugá-la com data limite errada...");
        houseOfCards.rent(until);
        System.out.println("Está disponível: " + houseOfCards.getAvailable());
        System.out.println("Indisponível até: " + houseOfCards.getUnavailableUntil() + "\n\n");

        LocalDate until2 = LocalDate.parse("2022-11-30");
        System.out.println("\nTentando alugá-la de novo, com data limite correta...");
        houseOfCards.rent(until2);
        System.out.println("Está disponível: " + houseOfCards.getAvailable());
        System.out.println("Indisponível até: " + houseOfCards.getUnavailableUntil() + "\n\n");

        // ########################################################################################
        // ########################################################################################
        // ########################################################################################
        // ########################################################################################

        System.out.println("\nTerceiro exemplo - Reforma");
        RealState laCasaDePapel = new RealState(2, 44, 42323.32F, "rua dos bobos 0", true);
        System.out.println("Endereço: " + laCasaDePapel.getAdress());
        System.out.println("Area construída: " + laCasaDePapel.getNumPavements());
        System.out.println("Número de pavimentos: " + laCasaDePapel.getArea());
        System.out.println("Número de comodos: " + laCasaDePapel.getNumRooms());
        System.out.println("Está disponível: " + laCasaDePapel.getAvailable());
        System.out.println("Indisponível até: " + laCasaDePapel.getUnavailableUntil() + "\n\n");

        LocalDate until3 = LocalDate.parse("2032-06-30");
        laCasaDePapel.reform(until3);
        System.out.println("Está disponível: " + laCasaDePapel.getAvailable());
        System.out.println("Indisponível até: " + laCasaDePapel.getUnavailableUntil() + "\n\n");
    }
}
