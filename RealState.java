import java.time.LocalDate;

public class RealState {

    private int numPavements;
    private int numRooms;
    private float area;
    private String adress;
    private boolean isAvailable;
    private LocalDate isUnavailableUntil;
    private boolean isRented;
    private boolean isSold;
    private boolean isReforming;

    public RealState(int numPavements, int numRooms, float area, String adress, boolean isAvailable) {
        setNumPavements(numPavements);
        setNumRooms(numRooms);
        setArea(area);
        setAdress(adress);
        setAvailable(isAvailable);
    }

    // Dúvida pra tirar com o Leo.. Como proceder nesses casos? Esse é um método pra
    // refreshar sempre que a pessoa der um get na disponibilidade. Tem que criar um
    // método separado?
    public void refresh() {
        if (!this.getAvailable()) {
            if (!todayIsBefore(this.getUnavailableUntil())) {
                this.setAvailable(true);
                this.setUnavailableUntil(null);
            }
        }
    }

    public void sell() {
        System.out.println("Vendendo imóvel...");
        if (this.getAvailable()) {
            this.setSold(true);
            System.out.println("Imóvel vendido!");
        } else {
            System.out.println("Imóvel nao disponível. Venda cancelada.");
        }
    }

    public void rent(LocalDate until) {
        System.out.println("Alugando imóvel...");
        if (this.getAvailable()) {
            if (todayIsBefore(until)) {
                System.out.println("Imóvel alugado!");
                this.setRented(true);
                this.setUnavailableUntil(until);
            } else {
                System.out.println("Data inválida. Aluguel cancelado.");
            }
        } else {
            System.out.println("Imóvel nao disponível. Aluguel cancelado.");
        }
    }

    public void reform(LocalDate until) {
        System.out.println("Colocando imóvel para reforma...");
        if (this.getAvailable()) {
            if (todayIsBefore(until)) {
                System.out.println("O imóvel entrou em reforma!");
                this.setReforming(true);
                this.setUnavailableUntil(until);
            } else {
                System.out.println("Data inválida. Reforma cancelada.");
            }
        } else {
            System.out.println("Imóvel nao disponível. Reforma cancelada");
        }
    }

    public void setNumPavements(int numPavements) {
        this.numPavements = numPavements;
    }

    public int getNumPavements() {
        return this.numPavements;

    }

    public void setNumRooms(int numRooms) {
        this.numRooms = numRooms;
    }

    public int getNumRooms() {
        return this.numRooms;

    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getArea() {
        return this.area;

    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getAdress() {
        return this.adress;

    }

    public void setAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean getAvailable() {
        return this.isAvailable;

    }

    public void setUnavailableUntil(LocalDate date) {
        this.isUnavailableUntil = date;
    }

    public LocalDate getUnavailableUntil() {
        return this.isUnavailableUntil;
    }

    public void setSold(boolean isSold) {
        this.isSold = isSold;
        this.setAvailable(false);
    }

    public boolean getSold() {
        return this.isSold;
    }

    public void setRented(boolean isRented) {
        this.isRented = isRented;
        this.setAvailable(false);
    }

    public boolean getRented() {
        return this.isRented;
    }

    public void setReforming(boolean isReforming) {
        this.isRented = isReforming;
        this.setAvailable(false);
    }

    public boolean getReforming() {
        return this.isReforming;
    }

    public boolean todayIsBefore(LocalDate date) {
        return (LocalDate.now().isBefore(date)) ? true : false;
    }
}